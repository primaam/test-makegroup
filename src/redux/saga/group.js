import {takeLatest, put} from 'redux-saga/effects'
import {saveToken, getHeaders, removeToken} from '../common/function/index'
import {apiGroup} from '../common/api/group'

function* makeGroup(action){
    try{
        const headers = yield getHeaders()
    
        const resGroup = yield apiGroup(headers,action.payload);
        yield put({type: 'ADD_GROUP_SUCCESS', payload: resGroup.data})
        console.log('berhasil')
    }catch(e) {
        console.log(JSON.stringify(e))
    }
}

function* groupSaga(){
    yield takeLatest('ADD_GROUP',makeGroup);
}
export default groupSaga