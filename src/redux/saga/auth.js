import {takeLatest, put} from 'redux-saga/effects'
import {LOGIN, LOGIN_FAILED, LOGIN_SUCCESS, LOGOUT} from '../action/action_types'
import {saveToken, getHeaders, removeToken} from '../common/function/index'
import {apiLogin} from '../common/api/auth'

function* login(action){
    try{
        const resLogin = yield apiLogin(action.payload);
        yield saveToken(resLogin.data.data.token_access)
        console.log(resLogin.data.data.token_access)
        yield put ({type: LOGIN_SUCCESS, payload:resLogin.data.data.token_access})

    }catch(e) {
        console.log(JSON.stringify(e))
        yield put ({type: LOGIN_FAILED})
    }
}

function* logout(){
    try{
        yield removeToken();
    } catch(e){
        console.log(JSON.stringify(e))
    }
}

function* authSaga(){
    yield takeLatest(LOGIN, login);
    yield takeLatest(LOGOUT, logout)
}
export default authSaga