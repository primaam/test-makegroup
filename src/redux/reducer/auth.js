import {LOGIN, LOGIN_FAILED, LOGIN_SUCCESS, LOGOUT} from '../action/action_types'

const initialState = {
    isLoading: false,
    isLoggedIn: false,
    data: {}
}

const auth = (state=initialState, action)=> {
    switch(action.type){
        case LOGIN:{
            return {
                ...state,
                isLoading: true
            }
        }
        case LOGIN_SUCCESS: {
            return{
                isLoading: false,
                isLoggedIn: true,
                data: action.payload
            }
        }
        case LOGIN_FAILED: {
            return{
                isLoading: false,
                isLoggedIn: false
            }
        }
        case LOGOUT: {
            return{
                ...state,
                isLoading: false,
                isLoggedIn: false
            }
        }
        default:
            return{
                ...state
            }
    }
}
export default auth