const initialState ={
    data:{}
}

const group = (state=initialState, action) =>{
    switch (action.type){
        case 'ADD_GROUP':{
            return{
                ...state
            }
        }
        case 'ADD_GROUP_SUCCESS': {
            return {
            ...state,
            data: action.payload
            }
        }
        default:
            return{
                ...state
            }
    }
}

export default group