import axios from 'axios'

export function apiLogin(payload) {
    return axios ({
        method: 'POST',
        url: "http://bta70.omindtech.id/api/tentor/login",
        data: payload
    })
}