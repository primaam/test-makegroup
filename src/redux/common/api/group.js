import axios from 'axios'
import {useSelector} from 'react-redux'

export function apiGroup( headers, payload){
    return axios({
        method: 'POST',
        url: "http://bta70.omindtech.id/api/grup",
        data: payload,
        headers
    })
}