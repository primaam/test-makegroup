import AsyncStorage from '@react-native-async-storage/async-storage'

export async function getHeaders(){
    const token = await AsyncStorage.getItem('APP_AUTH_TOKEN')
    return {
        'Content-Type': 'application/json',
        Authorization: token
    }
}

export async function saveToken(token) {
    AsyncStorage.setItem('APP_AUTH_TOKEN', token);
}

export async function removeToken(token) {
    AsyncStorage.removeItem('APP_AUTH_TOKEN');
}