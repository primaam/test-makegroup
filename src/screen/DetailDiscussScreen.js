import React, { useState } from 'react'
import {widthPercentageToDP as wp, heightPercentageToDP as hp,} from 'react-native-responsive-screen';
import {View, Text, StyleSheet, Button, Image, TextInput, TouchableOpacity} from 'react-native'
import {Picker} from '@react-native-picker/picker'
import ImagePicker from 'react-native-image-picker'
import {useDispatch} from 'react-redux'


const options = {
    title: 'Select Avatar',
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };
export default function DetailDiscuss(){
    const [jenjang, setJenjang] = useState()
    const [image,setImage] = useState()
    const [rawImage, setRawImage] = useState({})
    const [name, setName] = useState('')

    const dispatch = useDispatch()
    function pickImage() {
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
            
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { 
                    uri: response.uri ,
                    type: response.type,
                    data: response.data,
                    name: response.fileName,
                };
            
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                setRawImage(source)
                setImage(response.uri)
            }
        });
    }

    const submit = () => {
        const data={
            kelas_id : jenjang,
            nama : name,
            thumbnail : image
        }
        dispatch({type: 'ADD_GROUP' ,payload:data})
    }

    return(
        <View>
            <View style={styles.image}> 
            {image == null ? 
            (
                <> 
                <Image 
                style={{width: wp('25%'), height: hp('12%')}}
                source={{uri:'https://www.btklsby.go.id/images/placeholder/basic.png'}}/>
                <TouchableOpacity onPress={() => pickImage()}>
                    <Text style={styles.text}>Tambahkan Foto Grup</Text>
                </TouchableOpacity> 
                </>
            ):(
                <Image 
                style={{width: wp('25%'), height: hp('12%')}}
                source={{uri:image}}/>
            )}
            </View>
            <View style={styles.inputData}>
                <TextInput 
                value={name}
                onChangeText={(text) => setName(text)}
                style={styles.inputText}
                placeholder="Nama Grup"/>
                <Picker 
                style={styles.inputJenjang}
                selectedValue={jenjang}
                mode='dropdown'
                onValueChange={(itemValue, itemIndex) => setJenjang(itemValue)}>
                    <Picker.Item label='Jenjang' value='Jenjang'/>
                    <Picker.Item label='1' value='1'/>
                    <Picker.Item label='2' value='2'/>
                    <Picker.Item label='3' value='3'/>
                </Picker>
            </View>
            <View style={styles.button}>
                <Button 
                onPress={()=> submit()}
                title='Submit'/>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    image: {
        marginTop: hp('3%'),
        alignItems: 'center',
        marginBottom: hp('4%')
    },
    text: {
        marginTop: hp('2%'),
        fontSize: hp('2%'),
        alignSelf: 'center',
    },
    inputData: {
        alignSelf: 'center',
        width: wp('80%')
    },
    inputText: {
        backgroundColor: '#F1F1F1',
        borderRadius: 10,
        marginBottom: hp('2%')
    },
    inputJenjang: {
        backgroundColor: '#F1F1F1',
        width: wp('40%'),
        borderRadius: 10,
        marginBottom: hp('2%')
    },
    button: {
        width: wp('80%'),
        alignSelf: 'center',
        borderRadius: 10, 
        padding: 10
    }
})
