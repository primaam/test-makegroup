import React, { useState } from 'react'
import {widthPercentageToDP as wp, heightPercentageToDP as hp,} from 'react-native-responsive-screen';
import {View, Text, StyleSheet, TextInput, Button} from 'react-native'
import { connect } from 'react-redux';
import {loginAction} from '../redux/action/auth'

function LoginScreen(props){
    const [email, setEmail] = useState('tentor@gmail.com')
    const [password, setPassword] = useState('123123')

    const login = () => {
        if(!email){
            alert ('Email wajib diisi')
        } else if (!password){
            alert ('Password wajib diisi')
        } else{
            props.processLogin({email, password})
        }
    }

    return(
        <View>
            <Text style={styles.text}>Login disini</Text>
            <TextInput 
            style={styles.input}
            value={email}
            onChangeText={(text)=> setEmail(text)}
            placeholder='Email'/>
            <TextInput 
            value={password}
            secureTextEntry={true}
            onChangeText={(text) => setPassword(text)}
            style={styles.input}
            placeholder='Password'/>
            <View style={{width: wp('40%'), alignSelf: 'center'}}>
                <Button 
                onPress={() => login()}
                style={styles.submit}
                title='Login'/>
            </View>
        </View>
    )
}
const mapStateToProps = (state) => ({
    isLoading: state.auth.isLoading
})
const mapDispatchToProps = (dispatch) => ({
    processLogin: (data) => dispatch(loginAction(data))
})

export default connect(mapStateToProps,mapDispatchToProps)(LoginScreen)
const styles = StyleSheet.create({
    text: {
        marginTop: hp('30%'),
        fontSize: hp('3%'),
        alignSelf: 'center',
    },
    input: {
        marginVertical: 10,
        borderWidth: 1,
        width: wp('70%'),
        alignSelf: 'center'
    },
    submit: {
        marginHorizontal: wp('5%')
    }
})