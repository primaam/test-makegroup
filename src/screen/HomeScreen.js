import React from 'react'
import {widthPercentageToDP as wp, heightPercentageToDP as hp,} from 'react-native-responsive-screen';
import {View, Text, StyleSheet} from 'react-native'

export default function HomeScreen(){
    return(
        <View>
            <Text style={styles.text}>Home Screen</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    text: {
        marginTop: hp('45%'),
        fontSize: hp('4%'),
        alignSelf: 'center',
    }
})
