import React from 'react'
import {widthPercentageToDP as wp, heightPercentageToDP as hp,} from 'react-native-responsive-screen';
import {View, Text, StyleSheet} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';

export default function DiscussScreen(props){
    return(
        <View>
            <TouchableOpacity onPress={() => props.navigation.navigate('Detail Discuss')}>
                <Text style={styles.text}>Discuss Screen</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    text: {
        marginTop: hp('45%'),
        fontSize: hp('4%'),
        alignSelf: 'center',
    }
})
