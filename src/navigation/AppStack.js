import React from 'react'
import {createStackNavigator} from '@react-navigation/stack'
import {NavigationContainer} from '@react-navigation/native';
import LoginScreen from '../screen/LoginScreen'
import MainNavigation from './MainNavigation';
import {useSelector} from 'react-redux'

const Stack = createStackNavigator();


function AppStack(props){
const isLoggedIn = useSelector((state) => state.auth.isLoggedIn)
    return(
        <NavigationContainer>
            <Stack.Navigator>
                {isLoggedIn ? (
                    <>
                    <Stack.Screen
                    options={{headerShown: false}}
                    name="Main"
                    component={MainNavigation}
                    />
                    </>
                ) : (
                    <Stack.Screen
                    options={{headerShown: false}}
                    name="Login"
                    component={LoginScreen}
                    />
                )}
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default AppStack