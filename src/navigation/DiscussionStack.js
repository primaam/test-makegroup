import React from 'react';
import DiscussScreen from '../screen/DiscussScreen';
import DetailDiscuss from '../screen/DetailDiscussScreen'
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();

export default function DiscussionStack(){
    return (
        <Stack.Navigator>
            <Stack.Screen
            name='Discuss'
            component={DiscussScreen}
            />
            <Stack.Screen
            name='Detail Discuss'
            component={DetailDiscuss}
            />
        </Stack.Navigator>
    )
}