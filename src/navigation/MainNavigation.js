import React from 'react';
import {Text} from 'react-native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HomeScreen from '../screen/HomeScreen'
import LiveScreen from '../screen/LiveScreen'
import ProfileScreen from '../screen/ProfileScreen'
import DiscussionStack from './DiscussionStack';
const Tab = createBottomTabNavigator();

export default function MainNavigation(props){
    return(
        <Tab.Navigator
        initialRouteName='Home'
        screenOptions={({route}) => ({
            tabBarLabel: ({focused, color, size}) => {
                return (
                <Text
                style={{fontSize: 10, color}}>
                {' '}{route.name}{' '}
                </Text>
                );
            }, 
            tabBarIcon: ({focused, color, size}) =>{
                let iconName;

                if (route.name === 'Home'){
                    iconName = focused ? 'home' : 'home-outline'
                } 
                else if (route.name === 'Live'){
                    iconName = focused ? "caret-forward-outline" : "caret-forward-outline"
                }
                else if (route.name === 'Discuss stack'){
                    iconName = focused ? "chatbubbles" : "chatbubbles-outline"
                }
                else if (route.name === 'Profil'){
                    iconName = focused ? "person" : "person-outline"
                }
                return <Ionicons name={iconName} size={size} color={color}/>
            },
        })}
        tabBarOptions={{
            activeTintColor: "#25C3D8",
            inactiveTintColor: "gray"
        }}
        >
            <Tab.Screen name="Home" component={HomeScreen} />
            <Tab.Screen name="Live" component={LiveScreen} />
            <Tab.Screen name="Discuss stack" component={DiscussionStack} />
            <Tab.Screen name="Profil" component={ProfileScreen} />
        </Tab.Navigator>
    )
}